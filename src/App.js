import './App.css';
import TodoList from './components/TodoList'
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {

  return (
    <div className="app">
      <div className='todos'>
        <TodoList/>
      </div>
    </div>
  );
}

export default App;
