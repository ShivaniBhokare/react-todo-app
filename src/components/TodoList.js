import React, { useEffect, useState } from 'react'
import TodoItem from './TodoItem'
import AddTodo from './AddTodo'
import axios from 'axios'

const TodoList = (props) => {
    const [todos,setTodos] = useState([])
    
    const [update,setUpdate] = useState(false)
    const doUpdate = () => setUpdate(prev => !prev)

    useEffect(() => {
        const fetchData = async () => {
            await axios.get('http://localhost:3000/todos')
            .then(res => setTodos(res.data))
            .catch(err => console.log(err))
        }
        fetchData()
    },[update]) 

    return(
    <>
        <h3>Todos</h3>

        <AddTodo doUpdate={doUpdate} />
        
        {todos.length > 0
        ? todos.map((todo,index) => (
            <TodoItem 
                key={todo.id}
                record={todo}
                doUpdate={doUpdate}
            />)
        )
        : <div className='emptylist'>Todo list is empty!</div>
        }

    </>
    )
}

export default TodoList