import axios from 'axios'
import React, { useState } from 'react'

const AddTodo = (props) => {
    const [error, setError] = useState(null)
    
    const [todo,setTodo] = useState({
        id: '',
        title: '',
        completed: false,
        canceled: false
    })

    const handleChange = (e) => {
        setTodo({...todo, [e.target.name]: e.target.value})
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        let pattern = RegExp(/^[a-z0-9 ]+$/i)
        let validTitle = pattern.test(todo.title)
        
        if (!todo.title.trim()) {
          setError('Todo Title required!')
          cancel()
        } 
        else if (!validTitle) {
            setError('Todo Title is inappropriate')
            cancel()
        }
        else {
            await axios.post(`http://localhost:3000/todos`,todo).then(res => {
                setTodo(res.data)
                props.doUpdate()
                setError(null)
                cancel()
            })
        }
        
    }

    const cancel = () => {
        setTodo({
            id: '',
            title: '',
            completed: false,
            canceled: false
        })
    }

    return(
        <>
            <form onSubmit={handleSubmit} className='formtodo'>
                <div>
                    <input className='todoinput' 
                        placeholder='Add todo item'
                        name='title' 
                        onChange={handleChange} 
                        value={todo.title} 
                        required
                        />
                    { error && <p className='errorclass'>{error}</p> }
                </div>
                <button type='submit' variant='primary' className='addbtn' title='Add new todo'>Add</button>
            </form>
        </>
    )
}

export default AddTodo