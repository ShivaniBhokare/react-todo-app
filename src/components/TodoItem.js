import React, { useState } from 'react'
import axios from 'axios'
import {FcCheckmark,FcCancel} from 'react-icons/fc'

const TodoItem = (props) => {
    const {
        id,
        title,
        completed,
        canceled
    } = props.record

    const [checkTodo,setCheckTodo] = useState({
        completed: false
    })

    const handleCheck = async (e) => {
        e.preventDefault()
        await axios.patch(`http://localhost:3000/todos/${id}`,checkTodo).then(
            setCheckTodo({
                completed: true
            })
        ).catch(err => {
            console.log(err)
        })
        props.doUpdate()
    }

    const handleDelete = async (e) => {
        e.preventDefault()
        await axios.delete(`http://localhost:3000/todos/${id}`).then(
            console.log('Deleted')
        ).catch(err => {
            console.log(err)
        })
        props.doUpdate()
    }

    return(
        <>
            <ul className='todolist'>
                <div className='todolistgroup'>
                    <li className='todolistitem' name='title' value={title} 
                        style={{
                            textDecoration: completed
                            ? "line-through"
                            : ""
                        }}>
                        {title}
                    </li>
                    {completed !== true
                        ? <FcCheckmark onClick={handleCheck} name='completed' value={completed} title='Mark complete' />
                        : null
                    }
                    <FcCancel onClick={handleDelete} name='canceled' value={canceled} title='Delete'></FcCancel>
                </div>
            </ul>
        </>
    )
}

export default TodoItem